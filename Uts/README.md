# ONEDRIVE 
<H2> Menampilkan use case table untuk produk digitalnya

### Tabel Users
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat mendaftar akun baru dengan username dan password | 100 |
| 2 | User dapat masuk ke akun OneDrive menggunakan username dan password | 100 |
| 3 | User dapat mengubah username dan password akun | 90 |
| 4 | User dapat menghapus akun OneDrive | 80 |
| 5 | User dapat melihat informasi profil akun | 90 |
| 6 | User dapat mengubah informasi profil akun seperti nama, alamat email, dan foto profil | 90 |

### Tabel Files
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat mengunggah file ke OneDrive | 100 |
| 2 | User dapat menghapus file dari OneDrive | 100 |
| 3 | User dapat mengunduh file dari OneDrive | 100 |
| 4 | User dapat membagikan file dengan pengguna lain | 90 |
| 5 | User dapat membuat salinan file | 80 |
| 6 | User dapat mencari file berdasarkan nama atau tipe file | 90 |

### Tabel Folders
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat membuat folder baru di OneDrive | 100 |
| 2 | User dapat menghapus folder dari OneDrive | 100 |
| 3 | User dapat mengubah nama folder di OneDrive | 90 |
| 4 | User dapat mengatur hak akses folder untuk pengguna lain | 90 |
| 5 | User dapat melihat isi folder dan file yang ada di dalamnya | 90 |
| 6 | User dapat memindahkan file atau folder ke lokasi lain di OneDrive | 90 |

### Tabel SharedFiles
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat menerima file yang dibagikan oleh pengguna lain | 100 |
| 2 | User dapat mengakses file yang dibagikan dengan hak akses yang diberikan | 100 |
| 3 | User dapat menghapus akses berbagi file | 90 |
| 4 | User dapat memberikan hak akses berbagi file kepada pengguna lain | 90 |
| 5 | User dapat melihat daftar file yang dibagikan olehnya kepada pengguna lain | 90 |
| 6 | User dapat melihat daftar file yang dibagikan oleh pengguna lain kepada dirinya | 90 |

### Tabel ActivityLog
| No | Use Case | Score |
| --- | --- | --- |
| 1 | Sistem mencatat aktivitas pengguna seperti mengunggah, menghapus, atau mengunduh file | 100 |
| 2 | User dapat melihat log aktivitas terkini di akun OneDrive | 90 |
| 3 | User dapat menghapus log aktivitas tertentu di akun OneDrive | 90 |
| 4 | User dapat mencari log aktivitas berdasarkan tanggal atau tipe aktivitas | 90 |
| 5 | Sistem memberikan notifikasi kepada pengguna tentang aktivitas terbaru di akun OneDrive | 80 |
| 6 | User dapat mengatur preferensi notifikasi untuk aktivitas di akun OneDrive | 80 |

## Diagram
![Dokumentasi](Dokumentasi/Diagrams_Database.png)

## Soal Interview UTS
NO | Pertanyaan | Jawaban
---|---|---
1 | Mampu mendemonstrasikan perancangan basis data berdasarkan permasalahan dunia nyata melalui reverse engineering produk digital global. Lampirkan bukti tabel hasil desain | [Jawaban 1](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%201.md#soal-1)
2 | Mampu mendemonstrasikan DATA DEFINITION LANGUAGE (DDL) secara tepat berdasarkan minimal 10 entitas dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record. | [Jawaban 2](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%202.md#soal-2)
3 | Mampu mendemonstrasikan DATA MANIPULATION LANGUAGE (DML) berdaraskan minimal 5 use case operasional (CRUD) dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record. | [Jawaban 3](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%203.md#soal-3)
4 | Mampu mendemonstrasikan DATA QUERY LANGUAGE (DQL) berdaraskan minimal 5 pertanyaan analisis dari produk digital yang dipilih, menggunakan setidaknya keyword GROUP BY, INNER JOIN, LEFT / RIGHT JOIN, AVERAGE / MAX / MIN. Lampirkan bukti berupa source code dan screen record. | [Jawaban 4](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%204.md#soal-4)
5 | Mampu mendemonstrasikan keseluruhan DDL, DML, dan DQL dari produk digital yang dipilih dalam bentuk video publik di Youtube. Lampirkan link Youtube | Menyusul

