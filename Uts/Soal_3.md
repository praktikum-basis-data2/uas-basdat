## Soal 3
Mampu mendemonstrasikan DATA MANIPULATION LANGUAGE (DML) berdaraskan minimal 5 use case operasional (CRUD) dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record.

1. Pengguna dapat membuat akun baru
```sql
INSERT INTO Users (user_id, username, email, password, registration_date, type_id):
VALUES (26, 'Asrul', 'skahyngproject@gmil.com', 'KingAsrul', '2024-03-20', 3);
```

2. Pengguna dapat mengganti naama file atau folder:
```sql
update folders 
set folder_name = 'Music'
where folder_id = 1;
```


3. pengguna dapaat menghapus file atau folder:
```sql
DELETE FROM files 
WHERE file_id = 12;
```


4. Pengguna dapaat melihat folder yang di miliki olehnya:
```sql
select folder_name 
from folders
where user_id = 1;
```

5. Mengurutkab activitylog dari terbaru dan aktifitas yang di lakukan:
```sql
select a.user_id, a.activity, a.log_date 
from activitylog a 
order by log_date;
```


